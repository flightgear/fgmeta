#!/bin/bash

SUBMODULES="simgear flightgear fgdata getstart"

if [ -z "$1" ]; then
  echo "usage: major.minor.patch"
  exit
fi

for f in $SUBMODULES .; do
  pushd "$f"
    git tag --force -a version/$1 -m "Version $1"\
    
    # delete existing tag
    git push origin :refs/tags/version/$1
    git push origin --tags
  popd
done

echo "Tagging meta"
git tag --force -a version/$1 -m "Version $1"

# delete existing tag
git push origin :refs/tags/version/$1
git push origin --tags
